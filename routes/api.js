var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var User = require("../models/user");
var searchIndex = require('search-index');
var options = {indexPath: 'searchIndex', logLevel: 'info'};
var dataset = require('../assets/tipuesearch_content_EN.json');
var Readable = require('stream').Readable;

router.post('/signup', function (req, res) {
  if (!req.body.username || !req.body.password) {
    res.status(200).json({success: false, msg: 'Please pass username and password.'});
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password
    });
    // save the user
    newUser.save(function (err) {
      if (err) {
        return res.status(200).json({success: false, msg: 'Username already exists.'});
      }
      res.status(200).json({success: true, msg: 'Successful created new user.'});
    });
  }
});

router.post('/signin', function (req, res) {
  User.findOne({
    username: req.body.username
  }, function (err, user) {
    if (err) throw err;

    if (!user) {
      res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          var token = jwt.sign(user, config.secret);
          res.status(200).json({success: true, token: 'JWT ' + token});
        } else {
          res.status(401).send({success: false, msg: 'Authentication failed. Username or Password is wrong.'});
        }
      });
    }
  });
});

router.get('/search', passport.authenticate('jwt', {session: false}), function (req, res) {
  var token = getToken(req.headers);
  if (token) {
    getSearch({
      search: req.query.search, pageSize: req.query.pageSize || 10, offset: req.query.offset || 1
    }, function (data) {
      res.status(200).json(data);
    })
  }
  else {
    return res.status(403).send({success: false, msg: 'Unauthorized.'});
  }
});

router.get('/searchWithoutAuth', function (req, res) {
  getSearch({
    search: req.query.search, pageSize: req.query.pageSize || 10, offset: req.query.offset || 1
  }, function (data) {
    res.status(200).json(data);
  })
});

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

const getFileStream = function (callback) {
  const s = new Readable({objectMode: true})
  dataset.pages.forEach(file => {
    s.push(file)
  })
  s.push(null)
  return s;
}

var vi;
searchIndex({indexPath: 'searchIndex', logLevel: 'error'}, function (err, myIndex) {
  getFileStream()
    .pipe(myIndex.defaultPipeline())
    .pipe(myIndex.add())
    .on('data', function () {
    })
    .on('end', function () {
      vi = myIndex;

    })

});

const getSearch = function (obj, cb) {
  var resData = [];
  vi.search({
    query: [{
      AND: {'*': [obj.search]}
    }],
    offset: obj.offset,
    pageSize: obj.pageSize
  }).on('data', function (response) {
    resData.push(response.document);
  }).on('end', function (response) {
    cb(resData);
  })
}

module.exports = router;
